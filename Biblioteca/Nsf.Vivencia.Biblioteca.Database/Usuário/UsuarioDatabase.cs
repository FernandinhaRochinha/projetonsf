﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.Vivencia.Biblioteca.Database
{
    public class UsuarioDatabase
    {
        public int Save (tb_usuario dto)
        {
            BibliotecaDB context = new BibliotecaDB();
            context.tb_usuario.Add(dto);
            return context.SaveChanges();
        }
        public void Update (int id)
        {
            BibliotecaDB context = new BibliotecaDB();
            tb_usuario user = context.tb_usuario.First(x => x.id_usuario == id);
            context.SaveChanges();
        }
        public void Remove (int id)
        {
            BibliotecaDB context = new BibliotecaDB();
            tb_usuario user = context.tb_usuario.First(x => x.id_usuario == id);
            context.tb_usuario.Remove(user);

        }
        public List<tb_usuario> List ()
        {
            BibliotecaDB context = new BibliotecaDB();
            return context.tb_usuario.ToList();
        }
    }
}
