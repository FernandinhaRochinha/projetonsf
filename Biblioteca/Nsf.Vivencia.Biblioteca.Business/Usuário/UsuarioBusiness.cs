﻿using Nsf.Vivencia.Biblioteca.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.Vivencia.Biblioteca.Business.Usuário
{
    public class UsurioDatabase
    {
        public int Save(tb_usuario dto)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.Save(dto);
        }
        public void Update(int id)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            db.Update(id);
        }
        public void Remove(int id)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            db.Remove(id);
        }
        public List<tb_usuario> List()
        {
            UsuarioDatabase db = new UsuarioDatabase();
            List<tb_usuario> list = db.List();

            return list;
        }
    }
}
