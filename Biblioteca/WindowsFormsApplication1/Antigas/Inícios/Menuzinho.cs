﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Novas_Telas.Cadastrar;
using WindowsFormsApplication1.Novas_Telas.Consultar;
using WindowsFormsApplication1.Telas.Cadastrar;

namespace WindowsFormsApplication1.Telas.NewFolder1
{
    public partial class MenuSecundário : Form
    {
        public static MenuSecundário Atual;

        public MenuSecundário()
        {
            InitializeComponent();
        }

        public void OpenScreen(UserControl control)
        {
            // Se a quantidades de CONTROLES for igual a 1, Ou seja, se já tiver uma tela dentro da GROUP BOX.
            if (PanelPrincipal.Controls.Count == 1)
                PanelPrincipal.Controls.RemoveAt(0);
            PanelPrincipal.Controls.Add(control);
            //Eu irei REMOVER o primeiro ÍNDICE do CONTROLE
            //Ou seja, sempre quando eu for abrir uma tela nova, ele vai remover a tela ATUAL e ADICIONA a NOVA.
        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
            CadastroUsuário tela = new CadastroUsuário();
            OpenScreen(tela);

            SidePanel.Height = btnUsuario.Height;
            SidePanel.Top = btnUsuario.Top;
        }

        private void btnLivro_Click(object sender, EventArgs e)
        {
            CadastroLivro frm = new CadastroLivro();
            OpenScreen(frm);

            SidePanel.Height = btnLivro.Height;
            SidePanel.Top = btnLivro.Top;

        }
        private void btnEmprestimo_Click(object sender, EventArgs e)
        {
            
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
           

        }

        private void btnEmprestimo_Click_1(object sender, EventArgs e)
        {
            CadastroEmpréstimo frm = new CadastroEmpréstimo();
            OpenScreen(frm);

            SidePanel.Height = btnEmprestimo.Height;
            SidePanel.Top = btnEmprestimo.Top;
        }

        private void btnInventario_Click_1(object sender, EventArgs e)
        {
            CadastrarInventário frm = new CadastrarInventário();
            OpenScreen(frm);

            SidePanel.Height = btnInventario.Height;
            SidePanel.Top = btnInventario.Top;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
            SidePanel.Height = btnDevolucao.Height;
            SidePanel.Top = btnDevolucao.Top;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            SidePanel.Height = btnAcervo.Height;
            SidePanel.Top = btnAcervo.Top;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MenuSecundário_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnBiblioteca_Click(object sender, EventArgs e)
        {
            
            SidePanel.Height = btnBiblioteca.Height;
            SidePanel.Top = btnBiblioteca.Top;
        }
    }
}
