﻿namespace WindowsFormsApplication1.Telas.Cadastrar
{
    partial class CadastrarInventário
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.nudMes = new System.Windows.Forms.NumericUpDown();
            this.nudPerda = new System.Windows.Forms.NumericUpDown();
            this.nudEntrega = new System.Windows.Forms.NumericUpDown();
            this.nudRepositores = new System.Windows.Forms.NumericUpDown();
            this.nudEmprestimos = new System.Windows.Forms.NumericUpDown();
            this.nudDoacoes = new System.Windows.Forms.NumericUpDown();
            this.nudPesquisas = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPerda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepositores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEmprestimos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDoacoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPesquisas)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(320, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 30);
            this.label1.TabIndex = 19;
            this.label1.Text = "Inventário";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.nudMes);
            this.groupBox1.Controls.Add(this.nudPesquisas);
            this.groupBox1.Controls.Add(this.nudDoacoes);
            this.groupBox1.Controls.Add(this.nudEmprestimos);
            this.groupBox1.Controls.Add(this.nudRepositores);
            this.groupBox1.Controls.Add(this.nudEntrega);
            this.groupBox1.Controls.Add(this.nudPerda);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(160, 130);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 278);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 21);
            this.label9.TabIndex = 57;
            this.label9.Text = "Entrega:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 21);
            this.label8.TabIndex = 56;
            this.label8.Text = "Repositores:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 21);
            this.label7.TabIndex = 55;
            this.label7.Text = "Emprestimos:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 21);
            this.label6.TabIndex = 54;
            this.label6.Text = "Doações:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 21);
            this.label5.TabIndex = 53;
            this.label5.Text = "Pesquisa:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 21);
            this.label4.TabIndex = 52;
            this.label4.Text = "Perda:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mês:";
            // 
            // btnConsultar
            // 
            this.btnConsultar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(0)))), ((int)(((byte)(28)))));
            this.btnConsultar.FlatAppearance.BorderSize = 0;
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.ForeColor = System.Drawing.Color.White;
            this.btnConsultar.Location = new System.Drawing.Point(342, 414);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(101, 27);
            this.btnConsultar.TabIndex = 22;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = false;
            // 
            // nudMes
            // 
            this.nudMes.Location = new System.Drawing.Point(143, 31);
            this.nudMes.Name = "nudMes";
            this.nudMes.Size = new System.Drawing.Size(194, 27);
            this.nudMes.TabIndex = 51;
            this.nudMes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudMes.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // nudPerda
            // 
            this.nudPerda.Location = new System.Drawing.Point(143, 64);
            this.nudPerda.Name = "nudPerda";
            this.nudPerda.Size = new System.Drawing.Size(194, 27);
            this.nudPerda.TabIndex = 51;
            this.nudPerda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudPerda.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // nudEntrega
            // 
            this.nudEntrega.Location = new System.Drawing.Point(143, 97);
            this.nudEntrega.Name = "nudEntrega";
            this.nudEntrega.Size = new System.Drawing.Size(194, 27);
            this.nudEntrega.TabIndex = 51;
            this.nudEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudEntrega.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // nudRepositores
            // 
            this.nudRepositores.Location = new System.Drawing.Point(143, 130);
            this.nudRepositores.Name = "nudRepositores";
            this.nudRepositores.Size = new System.Drawing.Size(194, 27);
            this.nudRepositores.TabIndex = 51;
            this.nudRepositores.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudRepositores.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // nudEmprestimos
            // 
            this.nudEmprestimos.Location = new System.Drawing.Point(143, 163);
            this.nudEmprestimos.Name = "nudEmprestimos";
            this.nudEmprestimos.Size = new System.Drawing.Size(194, 27);
            this.nudEmprestimos.TabIndex = 51;
            this.nudEmprestimos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudEmprestimos.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // nudDoacoes
            // 
            this.nudDoacoes.Location = new System.Drawing.Point(143, 196);
            this.nudDoacoes.Name = "nudDoacoes";
            this.nudDoacoes.Size = new System.Drawing.Size(194, 27);
            this.nudDoacoes.TabIndex = 51;
            this.nudDoacoes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudDoacoes.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // nudPesquisas
            // 
            this.nudPesquisas.Location = new System.Drawing.Point(143, 229);
            this.nudPesquisas.Name = "nudPesquisas";
            this.nudPesquisas.Size = new System.Drawing.Size(194, 27);
            this.nudPesquisas.TabIndex = 51;
            this.nudPesquisas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudPesquisas.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // CadastrarInventário
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnConsultar);
            this.Name = "CadastrarInventário";
            this.Size = new System.Drawing.Size(780, 528);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPerda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepositores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEmprestimos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDoacoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPesquisas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.NumericUpDown nudMes;
        private System.Windows.Forms.NumericUpDown nudPesquisas;
        private System.Windows.Forms.NumericUpDown nudDoacoes;
        private System.Windows.Forms.NumericUpDown nudEmprestimos;
        private System.Windows.Forms.NumericUpDown nudRepositores;
        private System.Windows.Forms.NumericUpDown nudEntrega;
        private System.Windows.Forms.NumericUpDown nudPerda;
    }
}
