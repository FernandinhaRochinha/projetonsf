﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Novas_Telas.Consultar;
using WindowsFormsApplication1.DB.ORM.Database.Bibliotecario;

namespace WindowsFormsApplication1.Novas_Telas.Cadastrar
{
    public partial class CadastroBibliotecario : UserControl
    {
        public CadastroBibliotecario()
        {
            InitializeComponent();
        }
        private tb_bibliotecario CarregarControles(tb_bibliotecario dto)
        {
            dto.nm_bibliotecario = txtNome.Text;
            dto.ds_cpf = txtCPF.Text;

            return dto;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            tb_bibliotecario bibi = new tb_bibliotecario();
            CarregarControles(bibi);

            BibliotecarioBusiness db = new BibliotecarioBusiness();
            db.Save(bibi);

            MessageBox.Show("Bibliotecario cadastrado com Sucesso!",
                            "NHA Biblioteca",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }
    }
}
