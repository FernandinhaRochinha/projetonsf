﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Devolucao
{
    public class DevolucaoDatabase
    {
        public int Save (tb_devolucao dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_devolucao.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_devolucao dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_devolucao devolve = context.tb_devolucao.First(x => x.id_devolucao == id);
            context.tb_devolucao.Remove(devolve);
            context.SaveChanges();
        }

        public List<tb_devolucao> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_devolucao.ToList();
        }

        public List<tb_devolucao> Filter(DateTime date)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_devolucao.Where(x => x.dt_devolucao == date).ToList();
        }
    }
}
