﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Punicao
{
    public class PunicaoDatabase
    {
        public int Save (tb_punicao punish)
        {
            LibraryDB context = new LibraryDB();
            context.tb_punicao.Add(punish);
            return context.SaveChanges();
        }

        public void Update(tb_punicao punish)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(punish).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int punish)
        {
            LibraryDB context = new LibraryDB();
            tb_punicao punir = context.tb_punicao.First(x => x.id_punicao == punish);
            context.SaveChanges();
        }

        public List<tb_punicao> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_punicao.ToList();
        }

        public List<tb_punicao> Filter(string punish)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_punicao.Where(x => x.ds_punicao.Contains(punish)).ToList();
        }
    }
}
