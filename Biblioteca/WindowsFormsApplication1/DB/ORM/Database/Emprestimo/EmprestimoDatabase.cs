﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database
{
    public class EmprestimoDatabase
    {
        public int Save(tb_emprestimo dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_emprestimo.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_emprestimo dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_emprestimo loan = context.tb_emprestimo.First(x => x.id_emprestimo == id);
            context.tb_emprestimo.Remove(loan);
            context.SaveChanges();
        }

        public List<tb_emprestimo> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_emprestimo.ToList();
        }

        //EXEMPLO DE CONSULTA POR APROXIMAÇÃO
        public List<tb_emprestimo> Filter(DateTime devolucao)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_emprestimo.Where(x => x.dt_devolucao == devolucao).ToList() ;
        }
    }
}
