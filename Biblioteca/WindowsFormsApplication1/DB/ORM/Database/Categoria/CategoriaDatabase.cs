﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Categoria
{
    public class CategoriaDatabase
    {
        public int Save(tb_categoria dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_categoria.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_categoria dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_categoria categoria = context.tb_categoria.First(x => x.id_categoria == id);
            context.tb_categoria.Remove(categoria);
            context.SaveChanges();
        }

        public List<tb_categoria> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_categoria.ToList();
        }

        public List<tb_categoria> Filter(string categoria)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_categoria.Where(x => x.nm_categoria.Contains(categoria)).ToList();
        }
    }
}
