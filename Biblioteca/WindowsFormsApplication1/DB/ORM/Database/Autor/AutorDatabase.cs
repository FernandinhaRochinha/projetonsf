﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Autor
{
    public class AutorDatabase
    {
        public int Save(tb_autor dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_autor.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_autor dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_autor autor = context.tb_autor.First(x => x.id_autor == id);
            context.tb_autor.Remove(autor);
            context.SaveChanges();
        }

        public List<tb_autor> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_autor.ToList();
        }

        public List<tb_autor> Filter(string autor)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_autor.Where(x => x.nm_autor.Contains(autor)).ToList();
        }
    }
}
