﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Bibliotecario
{
    public class GeneroDatabase
    {
        public int Save(tb_genero dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_genero.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_genero dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_genero genero = context.tb_genero.First(x => x.id_genero == id);
            context.tb_genero.Remove(genero);
            context.SaveChanges();
        }

        public List<tb_genero> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_genero.ToList();
        }

        public List<tb_genero> Filter(string genero)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_genero.Where(x => x.nm_genero.Contains(genero)).ToList();
        }
    }
}
