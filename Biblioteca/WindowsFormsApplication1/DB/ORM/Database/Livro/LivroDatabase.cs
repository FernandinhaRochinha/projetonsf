﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database
{
    public class LivroDatabase
    {
        public int Save(tb_livro dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_livro.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_livro dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_livro book = context.tb_livro.First(x => x.id_livro == id);
            context.tb_livro.Remove(book);
            context.SaveChanges();
        }

        public List<tb_livro> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_livro.ToList();
        }
        public List<tb_livro> Filter(string nome)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_livro.Where(x => x.nm_livro.Contains(nome)).ToList();
        }
    }
}
