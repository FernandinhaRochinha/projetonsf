﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Atraso
{
    public class AtrasoBusiness
    {
        public int Save(tb_atraso dto)
        {
            AtrasoDatabase db = new AtrasoDatabase();
            return db.Save(dto);
        }

        public void Update(tb_atraso dto)
        {
            AtrasoDatabase db = new AtrasoDatabase();
            db.Update(dto);
        }

        public void Remove (int id)
        {
            AtrasoDatabase db = new AtrasoDatabase();
            db.Remove(id);
        }

        public List<tb_atraso> List()
        {
            AtrasoDatabase db = new AtrasoDatabase();
            List<tb_atraso> list = db.List();

            return list;
        }
    }
}
