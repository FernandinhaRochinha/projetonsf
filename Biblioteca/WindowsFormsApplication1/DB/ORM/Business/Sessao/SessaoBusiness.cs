﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Sessao
{
    public class SessaoBusiness
    {
        public int Save(tb_sessao dto)
        {
            SessaoDatabase db = new SessaoDatabase();
            return db.Save(dto);
        }

        public void Update(tb_sessao dto)
        {
            SessaoDatabase db = new SessaoDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            SessaoDatabase db = new SessaoDatabase(););
            db.Remove(id);
        }

        public List<tb_sessao> List()
        {
            SessaoDatabase db = new SessaoDatabase();
            List<tb_sessao> list = db.List();

            return list;
        }

        public List<tb_sessao> Filter(string sessao)
        {
            SessaoDatabase db = new SessaoDatabase();
            List<tb_sessao> filter = db.Filter(sessao);

            return filter;
        }
    }
}
