﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Autor
{
    public class AutorBusiness
    {
        public int Save(tb_autor dto)
        {
            AutorDatabase db = new AutorDatabase();
            return db.Save(dto);
        }

        public void Update(tb_autor dto)
        {
            AutorDatabase db = new AutorDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            AutorDatabase db = new AutorDatabase();
            db.Remove(id);
        }

        public List<tb_autor> List()
        {
            AutorDatabase db = new AutorDatabase();
            List<tb_autor> list = db.List();

            return list;
        }

        public List<tb_autor> Filter(string autor)
        {
            AutorDatabase db = new AutorDatabase();
            List<tb_autor> filter = db.Filter(autor);

            return filter;
        }
    }
}
