﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.ORM.Database;

namespace WindowsFormsApplication1.DB.ORM.Business
{
    public class EmprestimoBusiness
    {
        public int Save(tb_emprestimo dto)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            return db.Save(dto);
        }

        public void Update(tb_emprestimo dto)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            db.Remove(id);
        }

        public List<tb_emprestimo> List()
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            List<tb_emprestimo> list = db.List();

            return list;
        }

        public List<tb_emprestimo> Filter(DateTime devolucao)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            List<tb_emprestimo> list = db.Filter(devolucao);

            return list;

        }
    }
}
