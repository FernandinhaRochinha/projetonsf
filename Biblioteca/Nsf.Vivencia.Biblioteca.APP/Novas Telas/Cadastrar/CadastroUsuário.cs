﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Novas_Telas.Consultar;
using WindowsFormsApplication1.Telas.NewFolder1;
using WindowsFormsApplication1.DB.Business;

namespace WindowsFormsApplication1.Novas_Telas.Cadastrar
{
    public partial class CadastroUsuário : UserControl
    {
        public CadastroUsuário()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            /*/
             * Então aqui nós iremos ter um problema.     
             * Uma User control é como uma tela filha... Logo a mesma é utilizada como meio de ser uma parte da tela. 
             * ´Portanto, ela só pode servir como instancia. Ela não pode chamar outras filhas, por conta disto, é necessário chamar outra Windows Form
             /*/
        }
        private void CarregarControles (tb_usuario dto)
        {
            dto.nm_nome = txtNome.Text;
            dto.ds_ano = Convert.ToInt32(txtAno.Text);
            dto.dt_nascimento = Convert.ToDateTime(txtNascimento.Text);
            dto.ds_email = txtEmail.Text;
            dto.ds_celular = txtCelular.Text;
            dto.ds_telefone = txtTelefone.Text;
            dto.ds_ra = txtRA.Text;
            dto.ds_numero = txtNumero.Text;
            dto.ds_cep = txtCEP.Text;
            dto.ds_complemento = txtComplemento.Text;
        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            tb_usuario user = new tb_usuario();
            
            CarregarControles(user);

            UsuarioBusiness db = new UsuarioBusiness();
            db.Save(user);

            MessageBox.Show("Usuário salvo com sucesso!", 
                            "Biblioteca Frei",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }
    }
}
