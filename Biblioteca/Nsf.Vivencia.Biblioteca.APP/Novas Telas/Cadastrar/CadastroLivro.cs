﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Novas_Telas.Consultar;

namespace WindowsFormsApplication1.Novas_Telas.Cadastrar
{
    public partial class CadastroLivro : UserControl
    {
        public CadastroLivro()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consulta_de_Livro frm = new Consulta_de_Livro();
            frm.Show();
            Hide();
        }
    }
}
