﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.Aluno;

namespace AlunoRA.Telas.Consultar
{
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            AlunoDTO dto = new AlunoDTO();
            dto.nome = txtaluno.Text;
            AlunoBusiness business = new AlunoBusiness();
            List<AlunoDTO> lista = business.Consultar(dto);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {

        }
    }
}
