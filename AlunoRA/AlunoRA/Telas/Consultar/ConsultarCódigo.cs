﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlunoRA.Telas.Consultar
{
    public partial class UserControl3 : UserControl
    {
        public UserControl3()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Imagens (*.png) |* .png";

            DialogResult resultado = dialog.ShowDialog();
           
            if (resultado == DialogResult.OK)
            {
                BarCode barcode = new BarCode();
                string dado = barcode.LerCodigoBarras(dialog.FileName);

                txtcodigo.Text = dado;
                ptbbarra.ImageLocation = dialog.FileName;
            }

            
        }
    }
}
