﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.Aluno;

namespace AlunoRA.Telas.Cadastrar
{
    public partial class CadastrarAluno : UserControl
    {
        public CadastrarAluno()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AlunoDTO dto = new AlunoDTO();
            dto.nome = txtnome.Text;
            dto.ano = Convert.ToInt32(txtano.Text);
            dto.nascimento = Convert.ToDateTime(txtnascimento.Text);
            dto.email = txtemail.Text;
            dto.celular = txtcelular.Text;
            dto.telefone = txttelefone.Text;
            dto.ra = txtra.Text;
            dto.cep = txtcep.Text;
            dto.complemento = txtcomplemento.Text;
            dto.numero = Convert.ToInt32(txtnumeor.Text);


            AlunoBusiness business = new AlunoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Aluno cadastrado com sucesso.", "Vivência Bruno", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
