﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.Aluno;

namespace AlunoRA.Telas.Aluno
{
    public partial class UserControl2 : UserControl
    {
        public UserControl2()
        {
            InitializeComponent();
            CarregarCombo();
        }

        public void CarregarCombo()
        {
            AlunoBusiness business = new AlunoBusiness();
            List<AlunoDTO> lista = business.Listar();

            cboAluno.ValueMember = nameof(AlunoDTO.ID);
            cboAluno.DisplayMember = nameof(AlunoDTO.nome);
            cboAluno.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void UserControl2_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog local = new SaveFileDialog();
            local.Filter = "Imagens (*.png)|*.png";


            DialogResult resultado = local.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                BarCode barcode = new BarCode();
                Image image = barcode.SalvarCodigoBarra(lblra.Text, local.FileName);
                imgbarra.Image = image;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
