﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlunoRA.d_b.ponto;
using AlunoRA.d_b.Aluno;

namespace AlunoRA.Telas.Cadastrar
{
    public partial class BaterSaida : UserControl
    {
        public BaterSaida()
        {
            InitializeComponent();
        }

        private void BaterSaida_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PontoDTO dto = new PontoDTO();
            dto.Id_Aluno = Convert.ToInt32(lblid.Text);
            dto.Entrada = DateTime.Now;

            PontoBusiness bs = new PontoBusiness();
            bs.Salvar(dto);

            MessageBox.Show("Ponto efetuado com sucesso.", "Vivência Bruno",
                               MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Imagens (*.png) |* .png";

            DialogResult resultado = dialog.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                BarCode barcode = new BarCode();
                string dado = barcode.LerCodigoBarras(dialog.FileName);

                lblcodigo.Text = dado;

            }

            AlunoDTO dto = new AlunoDTO();
            dto.ra = lblcodigo.Text.Trim();

            AlunoBusiness business = new AlunoBusiness();
            List<AlunoDTO> consultar = business.Consultar(dto);

            lblnome.Text = dto.nome;
            lblemail.Text = dto.email;
            lblano.Text = dto.ano.ToString();
            lblid.Text = dto.ID.ToString();
        }
    }
}
