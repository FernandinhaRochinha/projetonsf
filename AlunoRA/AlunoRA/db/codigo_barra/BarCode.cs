﻿using Spire.Barcode;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.db.codigo_barra
{
    public class BarCode
    {
        //Para conseguir fazer o controle por código de barra é necessário utilizar o plugin NuGET Spire.Barcode
        //É necessário também utilizar o Plugin Drawing e Drawinw.Imaging
        public Image SalvarCodigoBarra(string mensagem, string arquivo = null)
        {
            //Configuração do código de barras
            BarcodeSettings config = new BarcodeSettings();
            config.ShowText = false;            //Pelo o que eu percebi, indica se irá mostrar o código de barra como texto
            config.ShowTopText = false;         //Indica se irá mostrar a informação a frente de todos os outros controles
            config.ShowCheckSumChar = false;    //Indica se irá mostrar o código nos padrões Code18 e Ean128
            config.ShowTextOnBottom = false;    //Indica se irá mostrar o texto do código de barra no botão

            //Atribui valor ao Código de barra
            config.Data = mensagem;

            //Gera o código de barra
            BarCodeGenerator barcode = new BarCodeGenerator(config);
            Image code = barcode.GenerateImage();

            //Salva a imagem
            if (!string.IsNullOrEmpty(arquivo))
                code.Save(arquivo.Replace(".png", "") + ".png", ImageFormat.Png);

            //Retorna a imagem com o código de barras
            return code;
        }

        public string LerCodigoBarras(Image arquivo)
        {
            //Lê o código de barra
            string[] dados = BarcodeScanner.Scan(new Bitmap(arquivo)); //Possibilita os serviços de scaneamento de código de barra
            return dados[0];
        }

        public string LerCodigoBarras(string arquivo)
        {
            //Lê o código de barra
            string[] dados = BarcodeScanner.Scan(new Bitmap(arquivo)); //Possibilita os serviços de scaneamento de código de barra
            return dados[0];
        }
    }
}
