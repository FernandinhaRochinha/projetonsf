﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.ponto
{
    public class PontoDTO
    {
        public int ID{ get; set; }
        public DateTime Entrada{ get; set; }
        public DateTime Saida { get; set; }
        public DateTime Ponto{ get; set; }
        public string Observacoes{ get; set; }
        public int Id_Aluno{ get; set; }


    }
}
