﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.ponto
{
    public class View_ponto
    {
        public int ID { get; set; }

        public DateTime Entrada { get; set; }

        public DateTime Saida { get; set; }

        public DateTime Ponto { get; set; }

        public string Observacoes { get; set; }

        public string Aluno { get; set; }
    }
}
