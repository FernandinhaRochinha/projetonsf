﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.ponto
{
    public class PontoBusiness
    {
        PontoDatabase db = new PontoDatabase();

        public int Salvar(PontoDTO dto)
        {
            return db.Salvar(dto);
        }

        public void Alterar(PontoDTO dto)
        {
            db.Alterar(dto);
        }

        public void Remover(PontoDTO dto)
        {
            db.Remover(dto);
        }

        public List<PontoDTO> Listar()
        {
            return db.Listar();
        }

        public List<PontoDTO> ConsultarPorAluno(PontoDTO dto)
        {
            return db.ConsultarPorAluno(dto);
        }
        public List<View_ponto> ConsultarView(View_ponto dt)
        {
            return db.ConsultarView(dt);
        }
    }
}
