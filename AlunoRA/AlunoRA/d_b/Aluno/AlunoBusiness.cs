﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.Aluno
{
    class AlunoBusiness
    {
        public int Salvar(AlunoDTO dto)
        {
          
            AlunoDatabase db = new AlunoDatabase();
            return db.Salvar(dto);
        }
        public List<AlunoDTO> Consultar(AlunoDTO dto)
        {
            AlunoDatabase db = new AlunoDatabase();
            return db.Consultar(dto);
        }
        public List<AlunoDTO> Listar()
        {
            AlunoDatabase db = new AlunoDatabase();
            return db.Listar();
        }
    }
}
