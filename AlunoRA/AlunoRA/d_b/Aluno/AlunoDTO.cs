﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA.d_b.Aluno
{
    class AlunoDTO
    {
        public int ID { get; set; }

        public string  nome { get; set; }

        public DateTime nascimento { get; set; }

        public string email { get; set; }

        public string celular { get; set; }

        public string telefone { get; set; }

        public string ra { get; set; }

        public string cep { get; set; }

        public int numero { get; set; }

        public string complemento { get; set; }

        public int ano { get; set; }
    }
}
