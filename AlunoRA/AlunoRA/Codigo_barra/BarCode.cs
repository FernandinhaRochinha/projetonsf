﻿using Spire.Barcode;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlunoRA
{
    public class BarCode
    {        
        public Image SalvarCodigoBarra(string mensagem, string arquivo = null)
        {          
            BarcodeSettings config = new BarcodeSettings();
            config.ShowText = false;            
            config.ShowTopText = false;       
            config.ShowCheckSumChar = false;
            config.ShowTextOnBottom = false;             
            config.Data = mensagem;
         
            BarCodeGenerator barcode = new BarCodeGenerator(config);
            Image code = barcode.GenerateImage();
      
            if (!string.IsNullOrEmpty(arquivo))
                code.Save(arquivo.Replace(".png", "") + ".png", ImageFormat.Png);
           
            return code;
        }
        public string LerCodigoBarras(Image arquivo)
        {           
            string[] dados = BarcodeScanner.Scan(new Bitmap(arquivo)); 
            return dados[0];
        }

        public string LerCodigoBarras(string arquivo)
        {           
            string[] dados = BarcodeScanner.Scan(new Bitmap(arquivo));
            return dados[0];
        }
    }
}
