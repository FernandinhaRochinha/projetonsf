﻿//CREATE SCHEMA IF NOT EXISTS `PontoDB` DEFAULT CHARACTER SET utf8 ;
//USE `PontoDB` ;

//-- -----------------------------------------------------
//-- Table `PontoDB`.`tb_aluno`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `PontoDB`.`tb_aluno` (
//  `id_aluno` INT NOT NULL AUTO_INCREMENT,
//  `nm_nome` VARCHAR(45) NULL,
//  `dt_nascimento` DATETIME NULL,
//  `ds_email` NVARCHAR(50) NULL,
//  `ds_celular` VARCHAR(45) NULL,
//  `ds_telefone` VARCHAR(45) NULL,
//  `ds_ra` VARCHAR(45) NULL,
//  `ds_cep` VARCHAR(45) NULL,
//  `ds_numero` INT NULL,
//  `ds_complemento` VARCHAR(45) NULL,
//  `dt_ano` INT NULL,
//  PRIMARY KEY (`id_aluno`),
//  UNIQUE INDEX `id_aluno_UNIQUE` (`id_aluno` ASC))
//ENGINE = InnoDB;


//-- -----------------------------------------------------
//-- Table `PontoDB`.`tb_ponto`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `PontoDB`.`tb_ponto` (
//  `id_ponto` INT NOT NULL AUTO_INCREMENT,
//  `dt_entrada` DATETIME NULL,
//  `dt_saida` DATETIME NULL,
//  `ds_observacao` VARCHAR(60) NULL,
//  `tb_aluno_id_aluno` INT NOT NULL,
//  PRIMARY KEY (`id_ponto`),
//  UNIQUE INDEX `id_ponto_UNIQUE` (`id_ponto` ASC),
//  INDEX `fk_tb_ponto_tb_aluno1_idx` (`tb_aluno_id_aluno` ASC),
//  CONSTRAINT `fk_tb_ponto_tb_aluno1`
//    FOREIGN KEY (`tb_aluno_id_aluno`)
//    REFERENCES `PontoDB`.`tb_aluno` (`id_aluno`)
//    ON DELETE NO ACTION
//    ON UPDATE NO ACTION)
//ENGINE = InnoDB;


//-- -----------------------------------------------------
//-- Table `PontoDB`.`tb_barra`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `PontoDB`.`tb_barra` (
//  `id_barra` INT NOT NULL AUTO_INCREMENT,
//  `img_barra` LONGTEXT NULL,
//  `ds_codigo` INT NULL,
//  `id_aluno` INT NOT NULL,
//  PRIMARY KEY (`id_barra`),
//  UNIQUE INDEX `id_barra_UNIQUE` (`id_barra` ASC),
//  INDEX `fk_tb_barra_tb_aluno_idx` (`id_aluno` ASC),
//  CONSTRAINT `fk_tb_barra_tb_aluno`
//    FOREIGN KEY (`id_aluno`)
//    REFERENCES `PontoDB`.`tb_aluno` (`id_aluno`)
//    ON DELETE NO ACTION
//    ON UPDATE NO ACTION)
//ENGINE = InnoDB;


//SET SQL_MODE=@OLD_SQL_MODE;
//SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
//SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;